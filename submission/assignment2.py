import os
import re
import sys
import time

graphRE=re.compile("(\\d+)\\s(\\d+)")
edgeRE=re.compile("(\\d+)\\s(\\d+)\\s(\\d+)")

def graphToFloat(G):
    size = len(G)
    for i in xrange(size):
        for j in xrange(size):
            if G[i][j] != float('Inf'):
                G[i][j] = int(G[i][j])

def BellmanFord(G):
    # Fill in your Bellman-Ford algorithm here
    # The pathPairs will contain a matrix of path lengths:
    #    0   1   2
    # 0 x00 x01 x02
    # 1 x10 x11 x12
    # 2 x20 x21 x22
    # Where xij is the length of the shortest path between i and j

    graphToFloat(G[1])
    graph = G[1]
    numVertices = len(G[0])
    pathPairs=[]
    adjMatrix = []

    for i in xrange(numVertices):
        row = []
        for j in xrange(numVertices):
            if graph[i][j] != float('Inf'):
                row.append(j)
        adjMatrix.append(row)

    for s in xrange(numVertices):
        row = graph[s]
        row[s] = 0
        for i in xrange(1, numVertices):
            for u in xrange(numVertices):
                for v in adjMatrix[u]:
                    d = row[u] + graph[u][v]
                    if row[v] > d:
                        row[v] = d
        pathPairs.append(row)
        for u in xrange(numVertices):
            for v in adjMatrix[u]:
                if row[v] > row[u] + graph[u][v]:
                    print("Found a negative cycle")


    return pathPairs

def FloydWarshall(G):
    # Fill in your Floyd-Warshall algorithm here
    # The pathPairs will contain a matrix of path lengths:
    #    0   1   2
    # 0 x00 x01 x02
    # 1 x10 x11 x12
    # 2 x20 x21 x22
    # Where xij is the length of the shortest path between i and j

    graphToFloat(G[1])
    pathPairs=G[1]
    numVertices = len(G[1])

    for i in xrange(numVertices):
        pathPairs[i][i] = 0

    for k in xrange(numVertices):
        for i in xrange(numVertices):
            for j in xrange(numVertices):
                dist = pathPairs[i][k] + pathPairs[k][j]
                if pathPairs[i][j] > dist:
                    pathPairs[i][j] = dist

    return pathPairs

def readFile(filename):
    # File format:
    # <# vertices> <# edges>
    # <s> <t> <weight>
    # ...
    inFile=open(filename,'r')
    line1=inFile.readline()
    graphMatch=graphRE.match(line1)
    if not graphMatch:
        print(line1+" not properly formatted")
        quit(1)
    vertices=list(range(int(graphMatch.group(1))))
    edges=[]
    for i in range(len(vertices)):
        row=[]
        for j in range(len(vertices)):
            row.append(float("inf"))
        edges.append(row)
    for line in inFile.readlines():
        line = line.strip()
        edgeMatch=edgeRE.match(line)
        if edgeMatch:
            source=edgeMatch.group(1)
            sink=edgeMatch.group(2)
            if int(source) >= len(vertices) or int(sink) >= len(vertices):
                print("Attempting to insert an edge between "+str(source)+" and "+str(sink)+" in a graph with "+str(len(vertices))+" vertices")
                quit(1)
            weight=edgeMatch.group(3)
            edges[int(source)][int(sink)]=weight
    # TODO: Debugging
    #for i in G:
        #print(i)
    return (vertices,edges)

def writeFile(lengthMatrix,filename):
    filename=os.path.splitext(os.path.split(filename)[1])[0]
    outFile=open('output/'+filename+'_output.txt','w')
    for vertex in lengthMatrix:
        for length in vertex:
            outFile.write(str(length)+',')
        outFile.write('\n')


def main(filename,algorithm):
    algorithm=algorithm[1:]
    G=readFile(filename)
    # G is a tuple containing a list of the vertices, and a list of the edges
    # in the format ((source,sink),weight)
    pathLengths=[]
    if algorithm == 'b' or algorithm == 'B':
        pathLengths=BellmanFord(G)
    if algorithm == 'f' or algorithm == 'F':
        pathLengths=FloydWarshall(G)
    if algorithm == "both":
        start=time.clock()
        BellmanFord(G)
        end=time.clock()
        BFTime=end-start
        start=time.clock()
        FloydWarshall(G)
        end=time.clock()
        FWTime=end-start
        print("Bellman-Ford timing: "+str(BFTime))
        print("Floyd-Warshall timing: "+str(FWTime))
    writeFile(pathLengths,filename)

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("python bellman_ford.py -<f|b> <input_file>")
        quit(1)
    main(sys.argv[2],sys.argv[1])
